# Password Generator

Simple random password generator app, build with Gluon Mobile.

## Pre-requisites

- Java JDK 11

## Instructions 

The application can run on the JVM on desktop platforms:

```
./gradlew run
```

The same application can also run natively on any targeted OS, including Android, iOS, Linux, Mac and Windows.

### Linux & Android Builds

Requirements:

- Linux host
- Packages for development dependencies
- GraalVM

Install development packages:

```
sudo apt install libgtk-3-dev libavformat-dev libavutil-dev libavcodec-dev libasound2-dev libpango1.0-dev libxtst-dev
```

Install GraalVM and setup environment:

```
export GRAALVM_HOME="/opt/graalvm-ce-java11-20.3.0"
export JAVA_HOME="$GRAALVM_HOME"
export PATH="$GRAALVM_HOME/bin:$PATH"
```

To create a native image for your host, execute the following command:

```
./gradlew nativeBuild nativeRun
```

for Android

```
./gradlew -Pandroid nativeBuild nativeRun
```

### Mac & iOS Builds

Requirements:

- Apple host
- Xcode 11+
- libusbmuxd from Homebrew
- libimobiledevice from Homebrew

Install homebrew packages:

```
brew install --HEAD libusbmuxd
brew install --HEAD libimobiledevice
```

Install GraalVM and setup environment:

```
export GRAALVM_HOME="/opt/graalvm-ce-java11-20.3.0/Contents/Home/"
export JAVA_HOME="$GRAALVM_HOME"
export PATH="$GRAALVM_HOME/bin:$PATH"
```

To create a native image for Mac, execute the following command:

```
./gradlew nativeBuild nativeRun
```

for iOS 

```
./gradlew -Pios nativeBuild nativeRun
```

